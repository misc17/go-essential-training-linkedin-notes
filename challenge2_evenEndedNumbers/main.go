package main

import "fmt"

func main() {

	counter := 0

	for i := 1000; i <= 9999; i++ {
		for j := i; j <= 9999; j++ {
			total := i * j
			totalStr := fmt.Sprintf("%d", total)

			if totalStr[0] == totalStr[len(totalStr)-1] {
				counter++
			}
		}
	}

	// String in Quotes
	fmt.Printf("Counter = %d \n", counter)
}
