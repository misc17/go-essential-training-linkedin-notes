// This is a comment

package main

import "fmt"

func main() {
	for i := 0; i < 3; i++ {
		fmt.Println(i)
	}

	// Example of Break
	fmt.Println("----")

	for i := 0; i < 3; i++ {
		if i > 1 {
			break
		}

		fmt.Println(i)
	}

	// Example of Continue
	fmt.Println("----")

	for i := 0; i < 3; i++ {
		if i < 1 {
			continue
		}

		fmt.Println(i)
	}

	// Example of using For as a While Loop
	fmt.Println("----")

	a := 0
	for a < 3 {
		fmt.Println(a)
		a++
	}

	// Example of While True
	fmt.Println("----")

	b := 0
	for {
		// Exit condition
		if b > 2 {
			break
		}

		fmt.Println(b)
		b++
	}
}
