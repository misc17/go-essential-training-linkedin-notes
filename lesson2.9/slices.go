package main

import "fmt"

func main() {

	// Same type
	loons := []string{"bugs", "daffy", "taz"}
	fmt.Printf("loons = %v (type %T)\n", loons, loons)

	// Length
	fmt.Println("Length")
	fmt.Println(len(loons)) // 3
	fmt.Println("-----")

	// 0 Indexing
	fmt.Println("0 Indexing")
	fmt.Println(loons[1]) // daffy
	fmt.Println("-----")

	// Slices
	fmt.Println("Slicing")
	fmt.Println(loons[1:]) // [daffy taz]
	fmt.Println("-----")

	// For loop
	fmt.Println("For Loop")

	for i := 0; i < len(loons); i++ {
		fmt.Println(loons[i])
	}

	fmt.Println("-----")

	// Single value Range
	fmt.Println("Single Value Range")

	for i := range loons {
		fmt.Println(i)
	}

	fmt.Println("-----")

	// Double value range
	fmt.Println("Double Value Range")

	for i, name := range loons {
		fmt.Printf("%s \tat index: %d\n", name, i)
	}

	fmt.Println("-----")

	// Double value range, ignore index using '_'
	fmt.Println("Double value range, ignoring index")
	for _, name := range loons {
		fmt.Println(name)
	}

	fmt.Println("-----")

	// Appending to slice
	fmt.Println("Appending to sliceº")
	loons = append(loons, "elmer")

	fmt.Println(loons) // [bugs daffy taz elmer]
}
