package main

import "fmt"

func main() {

	nums := []int{16, 8, 42, 4, 23, 15}

	// We need to take the first value, since numbers can be negative,
	// and having an initial value of 0 would make the answer wrong.
	max := nums[0]

	for _, number := range nums[1:] {
		if max < number {
			max = number
		}
	}

	fmt.Println(max)
}
