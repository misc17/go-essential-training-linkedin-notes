// This is a comment

package main

import (
	"fmt"
	"strings"
)

func main() {
	for i := 0; i <= 20; i++ {
		output := ""

		if i%3 == 0 {
			output += "fizz "
		}
		if i%5 == 0 {
			output += "buzz "
		}

		if len(output) == 0 {
			output = fmt.Sprint(i)
		}

		//println("'" + strings.Trim(output, " ") + "'")
		println(strings.Trim(output, " "))
	}

}
